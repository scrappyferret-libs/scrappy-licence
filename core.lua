--- Required libraries.
local json = require( "json" )
local crypto = require( "crypto" )
local mime
if system.getInfo( "platform" ) == "html5" or system.getInfo( "platform" ) == "nx64" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
else
	mime = require( "mime" )
end

-- Localised values.

-- Localised functions.
local gsub = string.gsub
local sub = string.sub
local format = string.format
local encode = json.encode
local decode = json.decode
local insert = table.insert
local digest = crypto.digest
local sha512 = crypto.sha512
local floor = math.floor
local encode = json.encode
local decode = json.decode
local b64 = mime.b64
local unb64 = mime.unb64
local pathForFile = system.pathForFile
local open = io.open
local close = io.close
local DocumentsDirectory = system.DocumentsDirectory
local remove = os.remove

-- Localised values.

-- Static values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Set the identifier
	self:setIdentifier( self._params.identifier )

	-- Set the length
	self:setLength( self._params.length )

end

--- Sets the identifier for the key.
-- @param identifier The identifier.
function library:setIdentifier( identifier )
	self._identifier = identifier
end

--- Gets the identifier for the key.
-- @return The identifier, or an empty string if none specified.
function library:getIdentifier()
	return self._identifier or ""
end

--- Sets the length for the key.
-- @param length The length.
function library:setLength( length )
	self._length = length
end

--- Gets the length of key to generate.
-- @return The length, or nil if none specified.
function library:getLength()
	return self._length
end

--- Generates an alpha numeric licence key.
-- @param data The data used for the key, string or a table.
-- @return The key.
function library:generate( data )
	return sub( digest( sha512, encode( data ) .. self:getIdentifier() ), 1, self:getLength() )
end

--- Verifies a licence key.
-- @param data The data to verify against.
-- @param key The key to verify.
-- @return True if it checks out, false otherwise.
function library:verify( data, key )
	return self:generate( data ) == gsub( key or "", "-", "" )
end

--- Formats a key into a more readable format.
-- @param key The key to format.
-- @param size The size of the groups to use for the formatting.
-- @return The formatted key.
function library:format( key, size )

	local hash = {}

	gsub( key, ".", function(c) insert( hash, c ) end )

	local key = ""

	for i = 1, #hash, 1 do
		key = key .. ( ( i % ( size or 0 ) == 0 and i ~= #hash ) and ( hash[ i ] .. "-" ) or hash[ i ] )
	end

	return key

end

--- Checks if a valid licence file is present.
-- @return True if it is, false otherwise.
function library:isRegistered()

	local path = pathForFile( "licence.key", DocumentsDirectory )

	local file, errorString = open( path, "r" )

	if not file then

		return false

	else

		local licence = unb64( gsub( file:read( "*a" ), "%s+", "" ) )

		licence = decode( licence )

	    close( file )

		file = nil

		return self:verify( licence.data, licence.key )

	end

end

--- Registers a licence key.
-- @param data The data for the licence.
-- @param key The key to register.
function library:register( data, key )

	local path = pathForFile( "licence.key", DocumentsDirectory )

	local file, errorString = open( path, "w" )

	if not file then

	else

		local licence = b64( encode( { data = data, key = key } ) )

	    file:write( licence )

	    close( file )

	end

	file = nil

end

--- Unregisters the licence key.
function library:unregister()
	remove( pathForFile( "licence.key", DocumentsDirectory ) )
end

--- Destroys this library.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Licence library
if not Scrappy.Licence then

	-- Then store the library out
	Scrappy.Licence = library

end

-- Return the new library
return library
